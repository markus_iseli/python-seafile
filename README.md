
python-seafile with support for Python 3
========================================

Cloned pull request from https://github.com/haiwen/python-seafile/pull/10

python client for seafile web api

Doc: https://github.com/haiwen/python-seafile/blob/master/doc.md
